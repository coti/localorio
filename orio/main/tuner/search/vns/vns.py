import orio.main.tuner.search.search
from orio.main.util.globals import *
import time, types

class VNS( orio.main.tuner.search.search.Search ):

    def __init__( self, params ):
        orio.main.tuner.search.search.Search.__init__(self, params)
        # Nothing special for the moment
        self._max_distance = 10 # TODO yuck

    def searchBestCoord( self, startCoord = None ):
        
        info( "\n----- begin variable neighborhood search -----" )

        if self.use_parallel_search:
            err( "Parallel VNS not implemented yet" )

        best_global_coord = None
        best_global_perf_cost = self.MAXFLOAT
        distance = 1
        visited = []
        
        # start the timer
        start_time = time.time()

        if None == startCoord:
            center = self.getRandomCoord()
        else:
            center = startCoord
        print "Center", center
        
        cost = self.getPerfCost( center ) 
        best_global_perf_cost = sum( cost )
        best_global_coord = center
        print "perf", cost

        while distance < self._max_distance and not ( self.time_limit > 0 and (time.time()-start_time) > self.time_limit ):

            disk = self.getNeighbors( center, distance )
        #    disk = self.getValidPoints( center, distance )
            if len( disk ) == 0:
                print "no valid point at distance", distance
                distance += 1
                continue
            #            disk = self.getAllValidPoints( center )
            neighbors = [ x for x in disk if x not in visited ]
            
            info( "VNS: distance %s  neighbors: %s \n" % ( str( distance ), len( neighbors ) ) )
            if 0 == len( neighbors ):
                break
            for n in neighbors:
                cost = self.getPerfCost( n ) 
                cost = sum( cost )
                info( "Cost of point %s: %s -- %s\n" % (n, cost, best_global_perf_cost ) )

                if cost < best_global_perf_cost:
                    best_global_coord = n
                    best_global_perf_cost = cost
                    info( "VNS found better point %s\n" % (n ) )
                
            visited = disk
            distance += 1
                
        # compute the total search time
        search_time = time.time() - start_time
        info( "Best point: %s\n" % (best_global_coord ) )
        
        info('----- end variable neighborhood search -----')
        
        # return the best coordinate
        return best_global_coord, best_global_perf_cost, search_time, distance
